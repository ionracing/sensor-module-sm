/* Includes ---------------------------------------- */
#include "stm32f4xx.h"

/* Function Prototypes ----------------------------- */
void BSP_ADC_init(uint8_t nbrOfChannels, uint32_t frequency, uint16_t oversampling);
void ADC_IRQHandler(void);
void TIM3_IRQHandler(void);

/* Defines ----------------------------------------- */

/*
Channel mapping. The DMA arranges the sensor channels in 
wierd order. Therefore some defines are used to make it more
understandable.
*/
#define ADC_CH_0 = 6
#define ADC_CH_1 = 0
#define ADC_CH_2 = 2
#define ADC_CH_3 = 4
#define ADC_CH_4 = 1
#define ADC_CH_5 = 3
#define ADC_CH_6 = 5

enum ADC_oversampling
{
	ADC_OVERSAMPLING_1    = 1,
	ADC_OVERSAMPLING_100  = 100,
	ADC_OVERSAMPLING_1000 = 1000,
};

enum ADC_frequency
{
	ADC_FREQ_50   = 50,
	ADC_FREQ_100  = 100,
	ADC_FREQ_200  = 200,
	ADC_FREQ_500  = 500,
	ADC_FREQ_1000 = 1000,
};

/* Macros ----------------------------------------- */
