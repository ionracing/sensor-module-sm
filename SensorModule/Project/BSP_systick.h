#include "stm32f4xx.h"

#define CLK_COMPLETE  0
#define CLK_RESET 1

void BSP_systick_init(void);

extern volatile uint16_t clk1000ms;
extern volatile uint16_t clk100ms;
extern volatile uint16_t clk10ms;
