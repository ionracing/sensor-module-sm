/*
 ID CAN bus list. 
 See the ID-CAN_BUS document on the drive / the documentation map
 for more information.
*/

#include "config.h"

/* 
Common Recieve IDs
*/
/* Errors (61->600) ---------------------------------------------------*/
/* Recieve */

/* Startupt (601->800)------------------------------------------------*/
/* recieve*/
#define ID_RX_CAL_SUSPENSION_MIN 601
#define ID_RX_CAL_THROTTLE_MAX 602
#define ID_RX_CAL_THROTTLE_MIN 603
#define ID_RX_CAL_BRAKE_MIN 604
#define ID_RX_CAL_BRAKE_MAX 605
/* Sensors messages (801->1100)---------------------------------------*/
/* Recieve */
#define ID_RX_REQUEST_SENSOR_DATA 801
/* Ping (1101->1150) -------------------------------------------------*/
#define ID_RX_PING
/* Messages (1151->2047)----------------------------------------------*/
/* Recieve */

/* 
Unique transmit IDs 
*/
/* Pedalbox defines ----------------------------------------- */
#ifdef SM_PEDALS
/* Errors */
/* Recieve */
/* Transmit */
#define ID_TX_ERROR_THROTTLE_IMPLASUABILITY 91

/* Sensors messages*/
/* Transmit */
#define ID_THROTTLE 806
#define ID_WHEEL_SPEED_FRONT 807
#define ID_BRAKE_PRESSURE_FRONT 808
#define ID_BRAKE_PRESSURE_BACK 809
#define ID_SUSPENSION_FRONT_L 810
#define ID_SUSPENSION_FRONT_R 811
#define ID_TEMPERATURE_FRONT 812
/* Startupt */
/* Transmit */
#define ID_TX_CAL_DONE 621

/* Messages*/
/* Recieve */
/* Transmit */

/* battery defines ---------------------------------------------------*/
#elif defined(SM_battery)
/* Errors*/
/* Recieve */
/* Transmit */

/* Sensors messages*/
/* Recieve */

/* Startupt*/
/* recieve*/
/* Transmit */

/* Sensors messages*/ 
/* Transmit */  

/* Messages */
/* Recieve */
/* Transmit */

/* Blackbox defines -----------------------------------------------------*/
#elif defined(SM_blackbox)
/* Errors */
/* Recieve */
/* Transmit */

/* Sensors messages */
/* Recieve */

/* Startupt */
/* recieve*/
/* Transmit */

/* Sensors messages */
/* Recieve */
/* Transmit */


/* Messages */
/* Recieve */
/* Transmit */
#endif
