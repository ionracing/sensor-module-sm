#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

#define ANALOG_ENABLE_PIN GPIO_Pin_12
#define REF_ENABLE_PIN GPIO_Pin_11

#define ANALOG_ERROR_PIN GPIO_Pin_9
#define DIGITAL_ERROR_PIN GPIO_Pin_10


void BSP_PWR_init()
{
	GPIO_InitTypeDef GPIO_initStruct;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_initStruct.GPIO_Pin = ANALOG_ENABLE_PIN | REF_ENABLE_PIN;
	GPIO_Init(GPIOC, &GPIO_initStruct);
	
	GPIO_initStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_initStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_initStruct.GPIO_Pin = ANALOG_ERROR_PIN | DIGITAL_ERROR_PIN;
	GPIO_Init(GPIOC, &GPIO_initStruct);
}


void BSP_PWR_analogON(void)
{
	GPIO_WriteBit(GPIOC, ANALOG_ENABLE_PIN, Bit_RESET);
	while(!GPIO_ReadInputDataBit(GPIOC, ANALOG_ERROR_PIN));
	GPIO_WriteBit(GPIOC, REF_ENABLE_PIN, Bit_RESET);
}


void BSP_PWR_analogOFF(void)
{
	GPIO_WriteBit(GPIOC, REF_ENABLE_PIN, Bit_SET);
	GPIO_WriteBit(GPIOC, ANALOG_ENABLE_PIN, Bit_SET);
}
