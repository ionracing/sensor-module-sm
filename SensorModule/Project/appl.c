/* Includes */
#include "stm32f4xx.h"
#include "BSP_LED.h"
#include "BSP_PWR.h"
#include "BSP_systick.h"
#include "BSP_IC.h"
#include "stm32f4xx_tim.h"
#include "BSP_ADC.h"
#include "stdbool.h"
#include "BSP_CAN.h"
#include "core_cm4.h"
#include "startup.h"
#include "BSP_ADC_SIMPLE.h"
#include "CAN_DEFINES.h"
#include "globals.h"

/* static variables */
#define NBR_OF_AIN 5
/*Function prototypes*/
static void heartbeat(void);
static void initSystem(void);
static void initSensorStructs(void);

/* Global variables */
uint8_t F_ecuDataRequest = RESET;
uint8_t F_driveMode = RESET;
// Sensors
AIN_typeDef throttle1;
AIN_typeDef throttle2;
AIN_typeDef brakePressure_Front;
AIN_typeDef brakePressure_Back;
AIN_typeDef temperatureSensor;
AIN_typeDef suspensionFront_R;
AIN_typeDef suspensionFront_L;


/* Main code */
int main(void)
{
	initSensorStructs();
	initSystem();	
	
	while(1)
	{
		/* Control loop transmit */
		if(F_ecuDataRequest == SET)
		{
			/* Sample all the sensors used in the control loop */
			throttle1.value 		  = BSP_ADC_SIMPLE_readChannel(throttle1.channel);
			throttle2.value 		  = BSP_ADC_SIMPLE_readChannel(throttle2.channel);
			suspensionFront_L.value   = BSP_ADC_SIMPLE_readChannel(suspensionFront_L.channel);
			suspensionFront_R.value   = BSP_ADC_SIMPLE_readChannel(suspensionFront_R.channel);
			brakePressure_Front.value = BSP_ADC_SIMPLE_readChannel(brakePressure_Front.channel);
			brakePressure_Back.value  = BSP_ADC_SIMPLE_readChannel(brakePressure_Back.channel);
			
			/* Process the raw data */
			
			/* Transmit the data */
			BSP_CAN_TxHalfword(throttle1.sensorID, 0, throttle1.value);
			BSP_CAN_TxHalfword(throttle2.sensorID, 0, throttle2.value);
			BSP_CAN_TxHalfword(suspensionFront_L.sensorID, 0, suspensionFront_L.value);
			BSP_CAN_TxHalfword(suspensionFront_R.sensorID, 0, suspensionFront_R.value);
			BSP_CAN_TxHalfword(brakePressure_Front.sensorID, 0, brakePressure_Front.value);
			BSP_CAN_TxHalfword(brakePressure_Back.sensorID, 0, brakePressure_Back.value);
		}
		heartbeat();
		
		/* 10 Hz logging */
		if(clk100ms == CLK_COMPLETE) 
		{
			/* Sample all the sensors used in the control loop */
			temperatureSensor.value = BSP_ADC_SIMPLE_readChannel(temperatureSensor.channel);
			
			clk100ms = CLK_RESET;
		}
		/* 100 Hz logging */
		if(clk10ms == CLK_COMPLETE)
		{
			clk10ms = CLK_RESET;
		}
	} 
} 



/* Misc functions *****************************************************/
static void heartbeat(void)
{
    if (clk1000ms == CLK_COMPLETE)
    {
        LED_TOGGLE(LED1);
        clk1000ms = CLK_RESET;
	}
}

/**
*	@brief	Inits Peripher-modules, watchdog, and so forth.
*/

static void initSystem(void)
{
	//BSP_IC_init();
	BSP_systick_init();
	BSP_LED_init();
    //BSP_ADC_init(NBR_OF_CHANNELS, SAMPLING_FREQUENCY, OVERSAMPLING);
	BSP_ADC_SIMPLE_init();
 // BSP_PWR_init();
 // BSP_PWR_analogOFF();
	BSP_CAN_init();
	//startup();

}
/**
* @brief configures the ADC inputs with an ID and data
*/
static void initSensorStructs(void)
{
	/* Init the sensors connected to the module*/
	throttle1.channel  = ADC_Channel_0;
	throttle1.calMax = 0;
	throttle1.calMin = 0;
	throttle1.value  = 0;
	throttle1.sensorID = ID_THROTTLE;
	
    throttle2.channel  = ADC_Channel_1;
	throttle2.calMax = 0;
	throttle2.calMin = 0;
	throttle2.value = 0;
	throttle2.sensorID = ID_THROTTLE;
	
	brakePressure_Front.channel  = ADC_Channel_2;
	brakePressure_Front.calMax = 0;
	brakePressure_Front.calMin = 0;
	brakePressure_Front.value = 0;
	brakePressure_Front.sensorID = ID_BRAKE_PRESSURE_FRONT;
	 
	brakePressure_Back.channel  = ADC_Channel_3;
	brakePressure_Back.calMax = 0;
	brakePressure_Back.calMin = 0;
	brakePressure_Back.value = 0;
	brakePressure_Back.sensorID = ID_BRAKE_PRESSURE_BACK;
	
	temperatureSensor.channel  = ADC_Channel_4;
	temperatureSensor.calMax = 0;
	temperatureSensor.calMin = 0;
	temperatureSensor.value = 0;
	temperatureSensor.sensorID = ID_TEMPERATURE_FRONT;

	suspensionFront_R.channel  = ADC_Channel_5;
	suspensionFront_R.calMax = 0;
	suspensionFront_R.calMin = 0;
	suspensionFront_R.value = 0;
	suspensionFront_R.sensorID = ID_SUSPENSION_FRONT_R;
	
	suspensionFront_L.channel  = ADC_Channel_6;
	suspensionFront_L.calMax = 0;
	suspensionFront_L.calMin = 0;
	suspensionFront_L.value = 0;
	suspensionFront_L.sensorID = ID_SUSPENSION_FRONT_L;
}