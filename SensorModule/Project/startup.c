/**
 * Sensor module startup code. The micro will stay in this mode
 * untill the car is ready to drive. The car is ready to drive; when the 
 * the ready to drive flag is set by the interface module.
 */
#include "stdbool.h"
#include "stdint.h"
#include "BSP_FLASH.h"
#include "BSP_LED.h"
#include "BSP_CAN.h"
#include "globals.h"
#include "BSP_ADC_SIMPLE.h"
/* Function prototypes */

/* Global Variables */ 
uint16_t sensorCalMax[8]; 
uint16_t sensorCalMin[8];

uint8_t F_requestCalMin = RESET;
uint8_t F_requestCalMax = RESET;
uint8_t requestedCalChannel = 0;

/* Private variables */

//TODO : make a system that calibrates all the, i.e throttle sensors, when
// A request for calibrating one of them is given.

void startup(void)
{
    BSP_FLASH_getCalibratedValues(sensorCalMax, sensorCalMin);
    while(F_driveMode == RESET)
    {
        if(F_requestCalMin == SET)
        {
            sensorCalMin[requestedCalChannel] = BSP_ADC_SIMPLE_readChannel();
            BSP_CAN_TxID(ID_TX_CAL_DONE);
            F_requestCalMin = RESET;
        }
        else if (F_requestCalMax == SET)
        {
            sensorCalMax[requestedCalChannel] = BSP_ADC_SIMPLE_readChannel();
            BSP_CAN_TxID(ID_TX_CAL_DONE);
            F_requestCalMax = RESET;
        }
    }
    // Store calibration values before the car starts driving.
    BSP_FLASH_storeCalibration(sensorCalMax, sensorCalMin);    
}

static void calibrateSensor(analog_typeDef* AIN)
{
    
}