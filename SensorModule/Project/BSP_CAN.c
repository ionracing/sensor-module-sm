#include "stdint.h"
#include "misc.h"
#include "stm32f4xx_can.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_rcc.h"
#include "BSP_ADC_SIMPLE.h"
#include "BSP_CAN.h"
#include "globals.h"

/* (on the RPI Communication board & on the STM32F4-discovery board) 
* PB8 : CAN1 RX  
* PB9 : CAN1 TX
*/

CanTxMsg txmsg;
CanRxMsg rxmsg;
uint8_t i = 0;

GPIO_InitTypeDef  GPIO_InitStructure;
CAN_InitTypeDef CAN_InitStructure;
NVIC_InitTypeDef NVIC_InitStructure;
CAN_FilterInitTypeDef CAN_FilterInitStructure;

void BSP_CAN_init(void) // CAN Setup
{ 
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;  //CAN RX: PB8,     CAN TX: PB9
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 8 & 9. 
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource8, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_CAN1);
	
	//CAN1 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN1 register init */
	CAN_DeInit(CAN1);

	/* CAN1 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    //1tq;

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/        //30MHz 1 MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler = (42000000 / 7) / 500000;                    //2;
	CAN_Init(CAN1, &CAN_InitStructure);

	/* CAN1 filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}
/**
* @brief Transmit a 16 bit long data over CAN-bus.
* @param ID :   The ID used for the message. Can be a uint32_t
                number,
* @param D0 :   uint8_t used if the message contains a identifier
                in the data 0 field.
* @param halfword   : The data to be transmitted.
*/
void BSP_CAN_TxHalfword(uint32_t ID, uint8_t D0 ,uint16_t halfword)
{
    uint8_t data[3];
    uint8_t length = 2;
    if(D0 == 0) // Only transmit ID and data
    {
        data[0] = MASK_LOWER_BYTE(halfword);
        data[1] = MASK_UPPER_BYTE(halfword);
        length = 2;
    }
    else
    {
        data[0] = D0;
        data[1] = MASK_LOWER_BYTE(halfword);
        data[2] = MASK_UPPER_BYTE(halfword);
        length = 3;
    }
    BSP_CAN_Tx(ID, length, data); 
}

void BSP_CAN_TxID(uint32_t ID)
{
    BSP_CAN_Tx(ID, 0, 0);
}

uint8_t BSP_CAN_Tx(uint32_t ID, uint8_t length, uint8_t data[8]) 
{
	txmsg.StdId = ID;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
	
	for(i=0; i<length;i++){
		txmsg.Data[i] = data[i];
	}
	return CAN_Transmit(CAN1, &txmsg);
}

#define RECIEVED_STARTUP_MESSAGE(ID) (ID > 600 && ID < 801)
#define RECIEVED_ERROR_MESSAGE(ID) (ID > 60 && ID < 601)
#define RECIEVED_SENSOR_MESSAGE(ID) (ID > 800 && ID < 1101)
#define RECIEVED_PING_MESSAGE(ID) (ID > 1100 && ID < 1151)
#define RECIEVED_MESSAGE(ID) (ID > 1150 && ID < 2048)

void CAN1_RX0_IRQHandler(void)
{
    if(CAN1->RF0R & CAN_RF0R_FMP0)
    {
        CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);

        if(RECIEVED_SENSOR_MESSAGE(rxmsg.StdId))
        {
            //Check only connected sensors :)
        }
        else if(RECIEVED_ERROR_MESSAGE(rxmsg.StdId))
        {
            // Nothing here yet
        }
        else if(RECIEVED_PING_MESSAGE(rxmsg.StdId))
        {
        }
        else if(RECIEVED_MESSAGE(rxmsg.StdId))
        {
            // Nothing here yet.
        }
        else if(RECIEVED_STARTUP_MESSAGE(rxmsg.StdId))
        {
            // Only check messages that corresponds to real sensors.
        }
    }
}	
       
