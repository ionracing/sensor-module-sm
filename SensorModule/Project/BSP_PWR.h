void BSP_PWR_init(void);
void BSP_PWR_analogON(void);
void BSP_PWR_analogOFF(void);
uint16_t BSPD_PWR_checkErrorState(void);
