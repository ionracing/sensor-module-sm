struct sensor
{
    uint8_t channel; // The physical channel it is connected to. 0->7.
    bool controlState; // True if the sensor is a part of the controll system.
    uint16_t sensorID; // The CAN-bus ID of the sensor.
};