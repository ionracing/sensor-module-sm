#include "stdbool.h"

/* constants */
#define SET 1
#define RESET 0
#define NBR_OF_ADC_CHANNELS 8

/* Flags */
/* Flags for calibrating sensor channels */
/* ECU Requests flags */
extern uint8_t F_ecuDataRequest; // ECU has requested sensor data
extern uint8_t F_driveMode; // The car state: either in armed or stopped.
extern uint8_t F_requestCalMin; // A minimum level calibration has been requested.
extern uint8_t F_requestCalMax; // A maximum level calibration has been requested.

/* RPI request Calibration */
/* 
0 : no request 
1 : channel 1
...
8  : channel 8
*/
extern uint8_t requestedCalChannel;

/* Sensor channel mapping */
typedef struct
{
    uint8_t channel;   // The physical channel it is connected to. 0->7
    uint16_t sensorID; // The CAN-bus ID of the sensor.
	uint16_t calMin; // Calibration value, min.
	uint16_t calMax; // Calibration value, max.
	uint16_t value;
	float processedValue;
}AIN_typeDef;

extern AIN_typeDef throttle1;
extern AIN_typeDef throttle2;
extern AIN_typeDef brakePressure_Front;
extern AIN_typeDef brakePressure_Back;
extern AIN_typeDef temperatureSensor;
extern AIN_typeDef suspensionFront_R;
extern AIN_typeDef suspensionFront_L;

// Wheel speed
extern float wheelFontRightRPM;
extern float wheelFrontleftRPM;
extern float wheelFrontAverage;

enum ADC_settings
	{
	NBR_OF_CHANNELS = 1,
	SAMPLING_FREQUENCY = 200,
	OVERSAMPLING = 1
	};

/* macros */
#define LENGTH_OF_ARRAY(array) sizeof(array)/sizeof(array[0])
