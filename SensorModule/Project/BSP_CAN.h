#include "CAN_DEFINES.h"

/* Function prototypes -----------------------------------------------*/
uint8_t BSP_CAN_Tx(uint32_t address, uint8_t length, uint8_t data[8]);
void BSP_CAN_TxID(uint32_t ID);
void BSP_CAN_TxHalfword(uint32_t ID, uint8_t D0 ,uint16_t halfword);    
void BSP_CAN_init(void);

/* MACROS */
#define MASK_LOWER_BYTE(data) (uint8_t)(data & 0xFF)
#define MASK_UPPER_BYTE(data) (uint8_t)(data >> 8)
